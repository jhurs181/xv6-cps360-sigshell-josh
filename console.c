// Console input and output.
// Input is from the keyboard or serial port.
// Output is written to the screen and serial port.

#include "types.h"
#include "defs.h"
#include "param.h"
#include "traps.h"
#include "spinlock.h"
#include "sleeplock.h"
#include "fs.h"
#include "file.h"
#include "memlayout.h"
#include "mmu.h"
#include "proc.h"
#include "x86.h"

#define IOCTL_CLEAR 7
#define IOCTL_MASK 42
#define SCREEN_HEIGHT 23
#define LINE_LENGTH 80

static int masks[4] = { 0, 0, 0, 0 };

static void consputc(int, int);

static int panicked = 0;

static struct {
  struct spinlock lock;
  int locking;
} cons;

static void
printint(int device, int xx, int base, int sign)
{
  static char digits[] = "0123456789abcdef";
  char buf[16];
  int i;
  uint x;

  if(sign && (sign = xx < 0))
    x = -xx;
  else
    x = xx;

  i = 0;
  do{
    buf[i++] = digits[x % base];
  }while((x /= base) != 0);

  if(sign)
    buf[i++] = '-';

  while(--i >= 0)
    consputc(buf[i], device);
}
//PAGEBREAK: 50

// Print to the console. only understands %d, %x, %p, %s.
void
cprintf(int device, char *fmt, ...)
{
  int i, c, locking;
  uint *argp;
  char *s;

  locking = cons.locking;
  if(locking)
    acquire(&cons.lock);

  if (fmt == 0)
    panic("null fmt");

  argp = (uint*)(void*)(&fmt + 1);
  for(i = 0; (c = fmt[i] & 0xff) != 0; i++){
    if(c != '%'){
      consputc(c, device);
      continue;
    }
    c = fmt[++i] & 0xff;
    if(c == 0)
      break;
    switch(c){
    case 'd':
      printint(device, *argp++, 10, 1);
      break;
    case 'x':
    case 'p':
      printint(device, *argp++, 16, 0);
      break;
    case 's':
      if((s = (char*)*argp++) == 0)
        s = "(null)";
      for(; *s; s++)
        consputc(*s, device);
      break;
    case '%':
      consputc('%', device);
      break;
    default:
      // Print unknown % sequence to draw attention.
      consputc('%', device);
      consputc(c, device);
      break;
    }
  }

  if(locking)
    release(&cons.lock);
}

void
panic(char *s)
{
  int i;
  uint pcs[10];

  cli();
  cons.locking = 0;
  cprintf(ALL_DEVS, "cpu with apicid %d: panic: ", cpu->apicid);
  cprintf(ALL_DEVS, s);
  cprintf(ALL_DEVS, "\n");
  getcallerpcs(&s, pcs);
  for(i=0; i<10; i++)
    cprintf(ALL_DEVS, " %p", pcs[i]);
  panicked = 1; // freeze other CPU
  for(;;)
    ;
}

//PAGEBREAK: 50
#define BACKSPACE 0x100
#define CRTPORT 0x3d4
static ushort *crt = (ushort*)P2V(0xb8000);  // CGA memory

static void
cgaputc(int c)
{
  int pos;

  // Cursor position: col + 80*row.
  outb(CRTPORT, 14);
  pos = inb(CRTPORT+1) << 8;
  outb(CRTPORT, 15);
  pos |= inb(CRTPORT+1);

  if(c == '\n')
    pos += 80 - pos%80;
  else if(c == BACKSPACE){
    if(pos > 0) --pos;
  } else
    crt[pos++] = (c&0xff) | 0x0700;  // black on white

  if(pos < 0 || pos > 25*80)
    panic("pos under/overflow");

  if((pos/80) >= 24){  // Scroll up.
    memmove(crt, crt+80, sizeof(crt[0])*23*80);
    pos -= 80;
    memset(crt+pos, 0, sizeof(crt[0])*(24*80 - pos));
  }

  outb(CRTPORT, 14);
  outb(CRTPORT+1, pos>>8);
  outb(CRTPORT, 15);
  outb(CRTPORT+1, pos);
  crt[pos] = ' ' | 0x0700;
}

// Helper function to determine if the mask should output '*'
int isprint(int c) {
  return (c > 0x1f && c != 0x7f && c != '\n');
}

void
consputc(int c, int device)
{
  if(panicked){
    cli();
    for(;;)
      ;
  }

  if(device == ALL_DEVS) {
    for(int i = 0; i < uartgetnumdevices(); i++) {
      if(c == BACKSPACE) {
        uartputc('\b', i); uartputc(' ', i); uartputc('\b', i);
      } else {
        uartputc((masks[i] && isprint(c)) ? '*' : c, i);
      }
    }
  } else {
    if(c == BACKSPACE) {
      uartputc('\b', device); uartputc(' ', device); uartputc('\b', device);
    } else {
      uartputc((masks[device] && isprint(c)) ? '*' : c, device);
    }
  }
  
  if(device == 0 || device == ALL_DEVS) {
    if(c == BACKSPACE) {
      cgaputc(c);
    } else {
      cgaputc((masks[0] && isprint(c)) ? '*' : c);
    }
  }
}

#define INPUT_BUF 128
struct {
  char buf[INPUT_BUF];
  uint r;  // Read index
  uint w;  // Write index
  uint e;  // Edit index
} input[4]; // One for each teletype

#define C(x)  ((x)-'@')  // Control-x

void
consoleintr(int (*getc)(int), int device)
{
  int c, doprocdump = 0;

  acquire(&cons.lock);
  while((c = getc(device)) >= 0){
    switch(c){
    case C('P'):  // Process listing.
      // procdump() locks cons.lock indirectly; invoke later
      doprocdump = 1;
      break;
    case C('U'):  // Kill line.
      while(input[device].e != input[device].w &&
            input[device].buf[(input[device].e-1) % INPUT_BUF] != '\n'){
        input[device].e--;
        consputc(BACKSPACE, device);
      }
      break;
    case C('H'): case '\x7f':  // Backspace
      if(input[device].e != input[device].w){
        input[device].e--;
        consputc(BACKSPACE, device);
      }
      break;
    default:
      if(c != 0 && input[device].e-input[device].r < INPUT_BUF){
        c = (c == '\r') ? '\n' : c;
        input[device].buf[input[device].e++ % INPUT_BUF] = c;
        consputc(c, device);
        if(c == '\n' || c == C('D') || input[device].e == input[device].r+INPUT_BUF){
          input[device].w = input[device].e;
          wakeup(&input[device].r);
        }
      }
      break;
    }
  }
  release(&cons.lock);
  if(doprocdump) {
    procdump();  // now call procdump() wo. cons.lock held
  } 
}

void consoleioctl(struct inode *ip, int key, int value) {
  if(key == IOCTL_CLEAR) {
    // if it's in a linux-style terminal, just send <ESC>c
    uartputc(27, ip->minor); // <ESC>
    uartputc(99, ip->minor); // 'c'
    
    // if it's in a QEMU emulated window, do some funky stuff
    for (int i = 0; i < (SCREEN_HEIGHT * 2); ++i) { // erase twice the window height
      cgaputc('\n');
    }

    for (int i = 0; i < (SCREEN_HEIGHT * LINE_LENGTH); ++i) { // then put the cursor back up one window's height (so it's at the top)
      cgaputc(BACKSPACE);
    }
  } else if(key == IOCTL_MASK) {
    masks[ip->minor] = value;
  }
}

int
consoleread(struct inode *ip, char *dst, int n)
{
  uint target;
  int c;

  iunlock(ip);
  target = n;
  acquire(&cons.lock);
  while(n > 0){
    while(input[ip->minor].r == input[ip->minor].w){
      if(proc->killed){
        release(&cons.lock);
        ilock(ip);
        return -1;
      }
      sleep(&input[ip->minor].r, &cons.lock);
    }
    c = input[ip->minor].buf[input[ip->minor].r++ % INPUT_BUF];
    if(c == C('D')){  // EOF
      if(n < target){
        // Save ^D for next time, to make sure
        // caller gets a 0-byte result.
        input[ip->minor].r--;
      }
      break;
    }
    *dst++ = c;
    --n;
    if(c == '\n')
      break;
  }
  release(&cons.lock);
  ilock(ip);

  return target - n;
}

int
consolewrite(struct inode *ip, char *buf, int n)
{
  int i;

  iunlock(ip);
  acquire(&cons.lock);
  for(i = 0; i < n; i++)
    consputc(buf[i] & 0xff, ip->minor);
  release(&cons.lock);
  ilock(ip);

  return n;
}

void
consoleinit(void)
{
  initlock(&cons.lock, "console");

  devsw[CONSOLE].write = consolewrite;
  devsw[CONSOLE].read = consoleread;
  devsw[CONSOLE].ioctl = consoleioctl;
  cons.locking = 1;

  picenable(IRQ_KBD);
  ioapicenable(IRQ_KBD, 0);
}
