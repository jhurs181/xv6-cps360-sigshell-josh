#include "types.h"
#include "stat.h"
#include "user.h"
#include "fs.h"
#include "fcntl.h"
void usage() {
	printf(1, "usage: mv SOURCE DEST\n");
	return;
}
int main(int argc, char* argv[]) {
	int source, dest;
	struct stat sourceStats;
	char* buff;
	//check we have both a source and destination argument
	if (argc < 3) {
		usage();
		exit();
	}
	//open source file to copy and make sure it opens succesfully
	source = open(argv[1], O_RDONLY);
	if (source < 0) {
		printf(1, "Error copying file. Cannot open source file, %s\n", argv[1]);
		exit();

	}
	// get the size of the source file and allocate a buffer to read it into.
	fstat(source, &sourceStats);
	buff = (char*)malloc(sourceStats.size);
	printf(1, "creating buffer of %d size\n", sourceStats.size);
	//read the contents of <source> into buffer
	int readAmt;
	readAmt = read(source, buff, sourceStats.size);
	printf(1, "Read %d bytes from source\n", readAmt);
	close(source);

	// open the destination file, createing it if necessary
	dest = open(argv[2], O_CREATE);
	if (dest < 0) {
		printf(1, "Error copying file. Could not create file, %s\n", argv[2]);
		free(buff);
		exit();
	}
	close(dest);
	dest = open(argv[2], O_WRONLY);
	if (dest < 0) {
		printf(1, "Error copying file\n");
		free(buff);
		unlink(argv[2]);
		exit();
	}
	//write data from source file to destination file.
	int writeAmt;
	writeAmt = write(dest, buff, readAmt);
	if (writeAmt < 0) {
		printf(1, "Failed to write data to destination file\n");
		unlink(argv[2]);
		exit();
	}
	printf(1, "Wrote %d bytes to dest\n", writeAmt);

	close(dest);
	free(buff);

	exit();
}
